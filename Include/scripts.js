var lastClicked = "";

// Drop down contents
const JOB_ABOUT_EMPLOYER = "aboutEmployer";
const JOB_DESC = "jobDesc";
const JOB_GOALS = "goals";
const JOB_CONCLUSION = "conclusion";

// Info dropdown buttons
const BUTTON_ABOUT = "aboutEmployerButton";
const BUTTON_DESC = "jobDescButton";
const BUTTON_GOALS = "jobGoalsButton";
const BUTTON_CONCLUSION = "jobConclusionButton";

const CCSMD_ABOUT_EMPLOYER = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspThe University of Guelph’s Computing and Communications department (abbreviated as CCS) provides IT services to the entire campus community. These include the " 
                        + "email and calendar systems, software distribution, registration systems, campus information security and much more. CCS was introduced to the University of Guelph to minimalize the need for outside " 
                        + "contractors by developing a suite of services that can be managed in-house to cut down on cost and time.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspThe team I was a part of was the CCS Managed Desktops team. Managed Desktops is responsible for providing various IT services to the university’s staff and "
                        + "infrastructure. Our core values are based around efficiency to ensure that issues/requests are handled effectively while having minimal impact on our client’s productivity. The Managed Desktops team "
                        + "is relatively small compared to the number of systems it is responsible for; comprised of around six technicians, four analysts and the manager, the team is responsible for roughly 1400 computer systems "
                        + "and clients located across the university’s campus.";
const CCSMD_JOB_DESC = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspWhile working at the University of Guelph’s CCS Managed Desktops team, my responsibility as an IT Technician was to provide a variety of IT services to "
                        + "clients on the university’s campus. Majority of my responsibility was to resolve any hardware or software related issue a client may have had on their computer. This could be anything from Microsoft "
                        + "Word not opening to software not being able to connect to a server, a hard drive failure, or encryption issues, etc.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspI also took part in project work. This was generally large scale requests from other teams or clients that usually takes a couple weeks depending on what needs "
                        + "to be done. One project I took part in was onboarding (preparing and transferring a new client’s machine on to our service) a department that had become our client. This entails installing various "
                        + "software, bringing the computer onto our domain, encrypting the computer then set the user up with their new account and give them a walk through of all the services we provide.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspTo be as efficient as possible when dealing with an issue, a knowledge of how computers function and interact is important while troubleshooting to determine "
                        + "that cause of the issue, then coming up with a fix for it. Communication and customer service skills were also very important as the nature of my role required me to constantly communicate with clients "
                        + "and other teams to come up with strategies and to gather as much info about the issue as possible. A lot of these skills and experience were developed while working at CCS, but a lot of my knowledge "
                        + "also came from my own personal use of computers and school. One course that saved me a few times was an operating systems course, which delved into how computers function and how components interact "
                        + "with each other.";
const CCSMD_GOALS = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspWhen I first started working at Managed Desktops, all the different technologies that were in use were a mystery, so I made it my first goal to understand "
                        + "how these technologies helped us provide the services we were responsible for. I found the best way to learn it was to get an overview of what it does from a co-worker, then to just dive in and "
                        + "attempt/practice using it on a test machine I had setup. Whenever I got stuck or didn’t know how to do something, my team was happy to assistance and tips. I now feel quite comfortable with using "
                        + "active directory and SCCM (System Center Configuration Manager) to distribute software, track information and perform administrative tasks. <br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAnother goal of mine was to improve my interpersonal skills when interacting with clients and other teams. I felt that at the beginning of my term I lacked "
                        + "the technical know how to describe issues to clients, but this got much better as I gained experience of how our systems work and the services we provide. I also became much better at interacting "
                        + "with teams to convey issues and what I thought about them. I also wanted to improve my customer service skills to ensure that our clients were satisfied with out service. When working through a "
                        + "solution, I would often do my best to show a client how I went about fixing the issue, things they could do to prevent it from occurring again and tips on how to improve their work flow. <br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMy third goal for the work term was to improve my own workflow to ensure I provide quality service. The first thing I did to achieve this was to set a priority " 
                        + "on tickets based on how much it affected a client’s productivity. While not being as fair as something like first come first serve, many clients were understanding and didn’t mind that I got to their "
                        + "ticket an hour later or so and allowed for me to keep productivity of my clients in general as high as possible. The second thing I did was to plan before contacting a client to think up a plan of "
                        + "attack and research the issue as much as possible so that when I do contact a client, the issue can be resolved as quickly as possible. As I gained more experience throughout the term, ticket turnaround "
                        + "time got shorter as I could think up more effective strategiesand troubleshooting methods.";
const CCSMD_CONCLUSION = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp I had a fantastic time working at CCS Managed Desktops and gained a whole lot of insight into the world of IT, the whole CCS team was awesome to work with! "
                        + "Before this work term, working a professional IT position was a mystery to me, but spending four months working with CCS taught me a large amount about how computers in an enterprise environment "
                        + "are managed, deepened my knowledge of computers in all aspects, introduced me to many new technologies and so much more. I have only scratched the surface of what the IT world has to offer and I "
                        + "look forward to learning a whole lot more! ";

const FEP_ABOUT_EMPLOYER = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspOriginating from Germany, Dematic, now a subsidiary of the KION Group is a global leader in designing, "
                        + "developing and delivering material handling solutions that optimize supply chains to improve performance and productivity. "
                        + "With offices in Europe, North America and Asia, Dematic provides these solutions to customers all over the world, including "
                        + "companies like Adidas, Under Armour and Amazon. Originating in 1819, Dematic has many years of experience in designing and "
                        + "implementing warehouse solutions and is an industry leader with an annual revenue of over 6 billion dollars. ";
const FEP_JOB_DESC = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspWhile working at Dematic, I was a part of the Facility Emulation Platform (FEP) team. "
                        + "The facility emulation team is responsible for creating an extensible 3D emulation of Dematic solutions, which are created "
                        + "on the Unity3D engine. This platform is utilized across the company for several reasons such as testing done by the performance "
                        + "analysis team, finding bugs in DiQ (software platform that handles automating facilties), and to demo proposed sites to clients to "
                        + "give them an idea of how their facility would look and perform beforehand. This emulation platform has also been extended to work "
                        + "in Virtual Reality to add an element that many other companies do not offer.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAs a member of the FEP team, I was tasked with working with the core team on a variety "
                        + "of different tasks such as UX and backend development. The highlight of my term was creating a component that was required for "
                        + "future sites as well as working on simulating workers doing their jobs throughout the facility with animations and such. This was "
                        + "especially awesome as the work I did would be used in public tradeshows and demoed to clients, so I felt that I was contributing to " 
                        + "the team in a big way. As well as working on the more public features, I was also tasked with work to implement features that would " 
                        + "be used by other teams such as tools for testing, as well as quality of life and improved UX features.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspTo be successful in this position, it was important to be an active member " 
                        + "of the team, requiring lots of communication with other members to ensure that any new additions to the platform move it in the "
                        + "right direction and follows the team plan. As well as effectively working as a team, being a solid programmer was important as well. "
                        + "There is a large emphasis on performance as sites can get especially large with thousands of 3D items in a site, as well as providing "
                        + "a smooth experience for users of the platform. The nature of the work is very close to that of video game development. These skills "
                        + "were definitely improved as the term progressed. Working with members whom were experienced in this sort of work, as well as the "
                        + "engineers and developers from other teams and other companies was extremely beneficial to my progress, giving me a variety of insights on problems. ";
const FEP_GOALS = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAs with working any new position and environment, I decided to set goals that would focus on becoming "
                        + "an asset to the team as soon as possible. Since the position I worked was responsible for emulating warehouse solutions in 3D, I would "
                        + "have to improve my knowledge of all the components that Dematic offers to ensure that my work is accurate. My plan to achieve this goal "
                        + "was to get a solid grasp on common/simpler components, like an accumulating conveyor for example, and then work my way up to more complex "
                        + "systems like automated sorters and shuttle systems. Understanding the flow of material handling was also crucial for accurately emulating " 
                        + "facilities. I could find multiple videos/wiki pages/documentation that provided great insight into the components and how they interacted with " 
                        + "other components. The other team members were also a huge help as they had tons of experience and were happy to answer any questions I had. " 
                        + "By the end of the term I feel that I reached my goal of gaining enough knowledge about facility emulation as I was able to provide a satisfactory "
                        + "functioning model of a component that would be used in future emulated sites and was successfully demoed to customers and other teams.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMy next goal was to improve my coding ability in terms of best practices, documentation and efficiency. "
                        + "Due to the nature of the work and it’s need for good performance I felt that achieving these goals were especially important to be successful "
                        + "in my position on the facility emulation team. My plan of action was to first learn the coding style and standards that the team enforces to "
                        + "ensure my code is maintainable and other members can instantly recognize things. This came with experience but also there was documentation "
                        + "available to me that I could reference whenever I needed too. To improve my coding solutions, I tried to research different algorithms I could "
                        + "use, as well as get feedback from other team members to ensure it is satisfactory. I also made sure to act on feedback given from code reviews "
                        + "as well. While this goal wasn’t something that was necessarily tangible, I feel that I improved a lot based on feedback from the team and became " 
                        + "a more reliable member as the work I was given increased in importance as the term went on.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMy final goal while working at Dematic was to make sure I was properly documenting my code and work to "
                        + "help team members quickly get a grasp of my work. This is important since I was a co-op for only four months and not a permanent employee. "
                        + "Dematic utilized wiki pages to contain all sorts of documentation across all teams and was the primary tool for gathering information. "
                        + "To do this my sub-goals were to help create any necessary wiki pages for work I took part in or to provide information to the team members "
                        + "who were responsible for documenting our teams work. This goal was unreached as I never took the initiative to provide documentation or contribute to wiki pages.";
const FEP_CONCLUSION = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspWorking at Dematic on the FEP team was a fantastic experience and will definitely go down as one "
                        + "of my favourite work terms. I learned a ton working as a developer with a highly skilled team and gave me invaluable experience. "
                        + "The role I was given made me feel that I was more than just a co-op, but also a core member of the team by being included in things "
                        + "like design decisions and planning. Coming out of this work term, I am much more confident in my abilities and I can apply everything "
                        + "I’ve learned from Dematic to any future endeavours.";

const SMARTSORT_ABOUT_EMPLOYER = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspBrock Solutions is an engineering solutions company specializing in the design, build and implementation of "
                        + "automated solutions for industrial/manufacturing, and transportation/logistics organizations around the world. "
                        + "Founded in Kitchener in 1992, Brock is an employee owned organization and now has over 400 employees all over North "
                        + "America with offices in Kitchener, Vancouver and Dallas.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspBrock’s solutions include consulting, manufacturing execution systems (MES), software automation, controls "
                        + "automation, panel fabrication and support/maintenance. Some examples are automated baggage systems in airports "
                        + "with systems at Toronto Pearson Airport and John F. Kennedy Airport, assembly lines in manufacturing factories, "
                        + "mail sortation and automation for Canada Post and much more.";
const SMARTSORT_JOB_DESC = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFor my work-term at Brock Solutions, I got the opportunity to work as a member of the SmartSort team. The SmartSort "
                        + "team is responsible for creating, customizing and configuring upper level controls for automated baggage solutions "
                        + "for airports, otherwise known as Brock’s “SmartSort” system. Each SmartSort system is custom made/tailored for the "
                        + "customer based on their various requirements. In my case, I first began development on Denver Airports new system, "
                        + "then moved over to the Australia project once Denver reached it’s testing phase. Both these projects were quite "
                        + "unique when compared to other projects since Denver is implementing an extremely large system with multiple "
                        + "subsystems and Australia is one of Brock’s first projects not in North America, thus having much different "
                        + "standards and requirements.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspFor both projects, I worked on backend and frontend components such as new web client features, creating new "
                        + "web client pages, gathering and displaying system statistics, development of the messaging interfaces, "
                        + "developing/configuring services used by SmartSort and much more. I really enjoyed working on the web client and "
                        + "backend messaging interfaces as I felt they are both a fundamental component of Brock’s solutions in different "
                        + "ways. The SmartSort web client is what is used by airport employees to control airport systems and view system "
                        + "information and the backend messaging is a core component of what makes the system work by communicating with the "
                        + "lower level systems.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspI found that to be successful in this position, it is very important to pay attention to and understand the "
                        + "designs and specifications provided by the customer as almost all the development and system logic is based "
                        + "off the provided specifications. Communication is also a key skill as the developers are also responsible for "
                        + "communicating with the customer’s technical teams in case clarifications are required or circumstances change, "
                        + "so being on the same page as the customer’s teams at all times helps ensure a successful solution.";
const SMARTSORT_GOALS = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMy first goal while working at Brock was to improve my technical writing skills. This is something I didn’t really "
                        + "have experience with prior to my work-term so I figured I would struggle with contributing to any documents or "
                        + "specifications. To make sure my contributions were up to quality, I looked at some older documents to get an idea "
                        + "of what was expected and make my additions followed the same style. It’s also really important to have good "
                        + "knowledge of the topic you are writing about, so I found that it was best to document my own work after I "
                        + "complete my task rather than leave it for someone else who may not have the same knowledge. I feel that I "
                        + "completed this goal as I made a few different contributions to the technical specification with minimal feedback "
                        + "from co-workers who reviewed it.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspA large part of my role involved working with large and complex SQL queries so I made it my goal to ensure "
                        + "that queries in terms of performance did not underperform. To pursue this goal, I would actively use a query "
                        + "profiler that would provide different performance statistics and then adjust my query based on those results. "
                        + "I would also research and compare different algorithms or methods to see which would apply best to my own needs; "
                        + "this definitely helped improve my SQL writing ability by broadening my knowledge of the language. From all the "
                        + "experience I gathered over the past eight months, I think I’ve achieved my goal of improving my SQL but I still "
                        + "feel that I have a way to go before I would consider myself amazing at it.<br><br>"
                        + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMy third goal working at Brock Solutions was to take part in a factory acceptance testing session (FAT). "
                        + "The FATs usually include all components of the system working together and being demoed to the customer to "
                        + "validate if requirements have been met and that everything works as intended. Unfortunately, the projects I "
                        + "worked on had their FATs take place in other locations and as co-op I was not able to travel so I did not get an "
                        + "opportunity to help present. I did however get to sit in on a FAT that was held in Kitchener so it was awesome "
                        + "to experience that and to see all the different components come together. I would say that this goal was half "
                        + "fulfilled since I did not actually get the chance to be an active participant, but being able to be a bystander "
                        + "of a different project’s FAT was great and I am sure that the experience gathered from it will benefit me sometime "
                        + "in the future whenever I have to demo something again.";
const SMARTSORT_CONCLUSION = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspIn conclusion, my eight-month work-term at Brock Solutions was packed with all sorts of experience. Coming out "
                        + "of this term, I feel that I have a better understanding of the processes that go into designing and developing a "
                        + "solution for a customer and improved/polished all my other skills like following technical specifications, "
                        + "communicating with the customer, coding and much more. It was awesome to know that the product I spent "
                        + "time working on is successful and is used on a scale as large as international airports. While I would have "
                        + "really enjoyed the opportunity to travel to experience the other side of the work the SmartSort team does, it’s "
                        + "understandable that this opportunity was not available during my time there.";

function displayText(clickedId, buttonId) {
    hideAllTextDropdowns();

    if (lastClicked.localeCompare(clickedId) == 0) {
        document.getElementById(clickedId).style.display = "none";
        inverseColours(buttonId, 0);
        lastClicked = "";
    } else {
        document.getElementById(clickedId).style.display = "inline";
        inverseColours(buttonId, 1);
        lastClicked = clickedId;
    }
}

function hideAllTextDropdowns() {
    document.getElementById(JOB_ABOUT_EMPLOYER).style.display = "none";
    inverseColours(BUTTON_ABOUT, 0);

    document.getElementById(JOB_DESC).style.display = "none";
    inverseColours(BUTTON_DESC, 0);

    document.getElementById(JOB_GOALS).style.display = "none";
    inverseColours(BUTTON_GOALS, 0);

    document.getElementById(JOB_CONCLUSION).style.display = "none";
    inverseColours(BUTTON_CONCLUSION, 0);

    lastClicked = "";
}

function inverseColours(button, inverse) {
    if (inverse == 0) {
        document.getElementById(button).style.color = "black";
        document.getElementById(button).style.backgroundColor = "white";
    } else if (inverse == 1) {
        document.getElementById(button).style.color = "white";
        document.getElementById(button).style.backgroundColor = "black";
    }
}

function setText(selectedWorkTerm) {
    switch (selectedWorkTerm) {
        case "managedDesktops": 
            document.getElementById(JOB_ABOUT_EMPLOYER).innerHTML = CCSMD_ABOUT_EMPLOYER;
            document.getElementById(JOB_DESC).innerHTML = CCSMD_JOB_DESC;
            document.getElementById(JOB_GOALS).innerHTML = CCSMD_GOALS;
            document.getElementById(JOB_CONCLUSION).innerHTML = CCSMD_CONCLUSION;
            break;
        
        case "facilityEmulation": 
            document.getElementById(JOB_ABOUT_EMPLOYER).innerHTML = FEP_ABOUT_EMPLOYER;
            document.getElementById(JOB_DESC).innerHTML = FEP_JOB_DESC;
            document.getElementById(JOB_GOALS).innerHTML = FEP_GOALS;
            document.getElementById(JOB_CONCLUSION).innerHTML = FEP_CONCLUSION;
            break;

        case "smartSort":
            document.getElementById(JOB_ABOUT_EMPLOYER).innerHTML = SMARTSORT_ABOUT_EMPLOYER;
            document.getElementById(JOB_DESC).innerHTML = SMARTSORT_JOB_DESC;
            document.getElementById(JOB_GOALS).innerHTML = SMARTSORT_GOALS;
            document.getElementById(JOB_CONCLUSION).innerHTML = SMARTSORT_CONCLUSION;
            break;
    }
}

function toggleMenu() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}